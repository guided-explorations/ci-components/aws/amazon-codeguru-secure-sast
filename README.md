# Amazon CodeGuru Secure SAST Scanner

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this components original canonical source project page! [Project Details](https://gitlab.com/guided-explorations/ci-components/aws/amazon-codeguru-secure-sast/)

[TOC]

## Usage

### Vendor Documentation

[Amazon CodeGuru Secure Documentation](https://docs.aws.amazon.com/codeguru/latest/security-ug/what-is-codeguru-security.html)

### Inputs and Configuration

| Input | Default value |    Type     | Description |
| ----- | ------------- | ----------- | ----------- |
| `CI_DEBUG_TRACE` | N/A | CI Variable     | Causes verbose component output for debugging. Set for pipeline or specific job. |
| `stage` | test | CI Component Input     | The CI Stage to run the component in. |
| `AWSCG_CONTAINER_TAG` | 'latest' | CI Component Input    | Which container version to peg to. So far AWS only publishes 'latest' |

### Smart Switching Between Security Dashboards and JUNIT Visualization

If you have GitLab Ultimate, the results will be treated like any other GitLab scanner and be visible in Security Dashboards, Merge Requests and available for Security Policy Merge Approvals.

If you have a lower license, the results will be available in JUNIT test visualizations in your pipeline results.

### Including

You should add this component to an existing `.gitlab-ci.yml` file by using the `include:`
keyword.

```yaml
include:
  - component: $CI_SERVER_FQDN/ci-components/aws/amazon-codeguru-secure-sast/codeguru-scan@<VERSION>
```
See "Components" tab in CI Catalog to copy exactly point to the current version.

where `<VERSION>` is the latest released tag or `main`.


### Working Example Code Using This Component

### AWS Account Access

#### Simple Auth With AWS Keys

Add the following GitLab CI/CD Variables to your project or a parent group:

  - AWS_ACCESS_KEY_ID (enable "masking" but not "protected")
  - AWS_SECRET_ACCESS_KEY (enable "masking" but not "protected")
  - AWS_DEFAULT_REGION (do not enable "protected")

#### Advanced Open ID Federation Security with OIDC and JWT

1. Follow this guide [Configure OpenID Connect in AWS to retrieve temporary credentials](https://docs.gitlab.com/ee/ci/cloud_services/aws/)
2. Add the following override code to your .gitlab-ci.yml. It will configure the CI component for OIDC.

```yaml
amazon_codeguru_sast:
  variables:
    ROLE_ARN: arn:aws:iam::828897360249:role/CodeGuruSecurityGitLabAccessRole #for CodeGuru to run all scans
    AWS_DEFAULT_REGION: "us-west-2" #for CodeGuru to run all scans
  id_tokens:
    MY_OIDC_TOKEN:
      aud: https://gitlab.com
```